package zeh.proj1;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import zeh.proj1.folhainformatica.Analista;
import zeh.proj1.folhainformatica.Diretor;
import zeh.proj1.folhainformatica.EmpresaInformatica;
import zeh.proj1.folhainformatica.Gerente;
import zeh.proj1.folhainformatica.Programador;
import zeh.proj1.utils.Escrita;
import zeh.proj1.utils.ImprimeEmArquivo;

/**
 * @author jose-almeida
 */
public class Teste {

  public static void main(String[] args) {
    EmpresaInformatica info = new EmpresaInformatica("Teste 1");

    info.addAnalista(new Analista(10, "ana", 1, 40, 8));
    info.addProgramador(new Programador(10, "prog", 1, 20, 8));
    info.addGerente(new Gerente("Ger", 1, 100000));
    info.addDiretor(new Diretor("Dir", 1, 200000));

    String file = "/home/jose-almeida/tst.txt";

    ImprimeEmArquivo arq = new ImprimeEmArquivo();
    arq.gerarFolhaPagamentoEmArquivo(file, info);

    Logger logger = Logger.getLogger(Teste.class.getName());

    try {
      Escrita esc = new Escrita();
      esc.carrega(file);

      logger.log(Level.INFO, info.gerarFolha());

    } catch (IOException ex) {
      logger.log(Level.SEVERE, null, ex);
    }
  }
}
