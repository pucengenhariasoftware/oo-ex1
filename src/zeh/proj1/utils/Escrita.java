package zeh.proj1.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import zeh.proj1.folhagenerica.Empresa;
import zeh.proj1.folhagenerica.Funcionario;
import zeh.proj1.folhainformatica.Analista;
import zeh.proj1.folhainformatica.Diretor;
import zeh.proj1.folhainformatica.EmpresaInformatica;
import zeh.proj1.folhainformatica.Gerente;
import zeh.proj1.folhainformatica.Programador;

/**
 *
 * @author jose-almeida
 */
public class Escrita {

  public static final String SEPARADOR = "::";
  public static final String SAIDA = "Empresa.obj";

  private static final Logger LOGGER = Logger.getLogger(Escrita.class.getSimpleName());

  private Empresa empresa;

  public void carrega(String caminho) throws IOException {
    this.carrega(new File(caminho));
  }

  public void carrega(File caminho) throws IOException {
    BufferedReader br = new BufferedReader(new FileReader(caminho));
    int indexLinha = 1;
    String line = br.readLine();

    this.carregaLinha(indexLinha, line);

    while (line != null) {
      line = br.readLine();

      if (line != null && !line.isEmpty()) {
        indexLinha++;
        this.carregaLinha(indexLinha, line);
      }
    }

    String saida = String.format("%s/%s", caminho.getParentFile().getAbsolutePath(), Escrita.SAIDA);
    this.salva(saida, this.empresa);
  }

  private void carregaLinha(int indexLinha, String linha) throws IOException, IllegalArgumentException {
    String[] partes = linha.split(Escrita.SEPARADOR);
    String tipo = partes[0];

    partes = Arrays.copyOfRange(partes, 1, partes.length);

    if (tipo.equals(Empresa.class.getSimpleName())) {
      if (this.empresa != null) {
        throw new IOException(String.format("Mais de uma empresa declarada - Linha %d", indexLinha));
      }

      this.empresa = this.carregaEmpresa(indexLinha, partes);

    } else if (tipo.equals(Funcionario.class.getSimpleName())) {
      if (this.empresa == null) {
        throw new IOException(String.format("Empresa ainda não encontrada - Linha %d", indexLinha));
      }

      Funcionario func = this.carregaFuncionario(indexLinha, partes);
      if (!this.empresa.addFuncionario(func)) {
        throw new IOException(String.format("Erro ao adicionar Funcionário a Empresa - Linha %d", indexLinha));
      }

    } else {
      throw new IOException(String.format("Tipo do registro não encontrado - Linha %d", indexLinha));
    }
  }

  private Empresa carregaEmpresa(int indexLinha, String[] partes) {
    String nome = partes[0];

    if (nome == null || nome.isEmpty()) {
      throw new IllegalArgumentException(String.format("Nome da Empresa não encontrado - Linha %d", indexLinha));
    }

    EmpresaInformatica resultado = new EmpresaInformatica();
    resultado.setNome(nome);

    return resultado;
  }

  private Funcionario carregaFuncionario(int indexLinha, String[] partes) {
    if (partes.length < 3) {
      throw new IllegalArgumentException(String.format("Funcionário precisa de Tipo, Nome e Código - Linha %d", indexLinha));
    }

    String tipo = partes[0];
    String nome = partes[1];
    int codigo = Integer.parseInt(partes[2]);

    partes = Arrays.copyOfRange(partes, 2, partes.length);

    if (tipo.equals(Analista.class.getSimpleName())) {
      return carregaAnalista(indexLinha, nome, codigo, partes);

    } else if (tipo.equals(Programador.class.getSimpleName())) {
      return carregaProgramador(indexLinha, nome, codigo, partes);

    } else if (tipo.equals(Gerente.class.getSimpleName())) {
      return carregaGerente(indexLinha, nome, codigo, partes);

    } else if (tipo.equals(Diretor.class.getSimpleName())) {
      return carregaDiretor(indexLinha, nome, codigo, partes);

    } else {
      throw new IllegalArgumentException(String.format("Tipo do funcionário não encontrado - Linha %d", indexLinha));
    }
  }

  private Funcionario carregaAnalista(int indexLinha, String nome, int codigo, String[] partes) {
    if (partes.length < 3) {
      throw new IllegalArgumentException(String.format("Analista precisa de Salário, Valor e Número de Horas - Linha %d", indexLinha));
    }

    double salario = Double.parseDouble(partes[0]);
    int valor = Integer.parseInt(partes[1]);
    int horas = Integer.parseInt(partes[2]);

    return new Analista(salario, nome, codigo, valor, horas);
  }

  private Funcionario carregaProgramador(int indexLinha, String nome, int codigo, String[] partes) {
    if (partes.length < 3) {
      throw new IllegalArgumentException(String.format("Programador precisa de Salário, Valor e Número de Horas - Linha %d", indexLinha));
    }

    double salario = Double.parseDouble(partes[0]);
    int valor = Integer.parseInt(partes[1]);
    int horas = Integer.parseInt(partes[2]);

    return new Programador(salario, nome, codigo, valor, horas);
  }

  private Funcionario carregaGerente(int indexLinha, String nome, int codigo, String[] partes) {
    if (partes.length < 1) {
      throw new IllegalArgumentException(String.format("Gerente precisa de Salário - Linha %d", indexLinha));
    }

    double salario = Double.parseDouble(partes[0]);
    return new Gerente(nome, codigo, salario);
  }

  private Funcionario carregaDiretor(int indexLinha, String nome, int codigo, String[] partes) {
    if (partes.length < 1) {
      throw new IllegalArgumentException(String.format("Diretor precisa de Salário - Linha %d", indexLinha));
    }

    double salario = Double.parseDouble(partes[0]);
    return new Diretor(nome, codigo, salario);
  }

  public void salva(String caminho, Empresa empresa) {
    this.salva(new File(caminho), empresa);
  }

  public void salva(File caminho, Empresa empresa) {
    try {
      FileOutputStream fileOut = new FileOutputStream(caminho);
      ObjectOutputStream objOut = new ObjectOutputStream(fileOut);

      objOut.writeObject(empresa);

    } catch (IOException ex) {
      Escrita.LOGGER.log(Level.SEVERE, null, ex);
    }
  }
}
