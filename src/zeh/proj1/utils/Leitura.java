package zeh.proj1.utils;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import zeh.proj1.folhagenerica.Empresa;

/**
 *
 * @author jose-almeida
 */
public class Leitura {

  private static final Logger LOGGER = Logger.getLogger(Leitura.class.getName());

  public Empresa ler(String caminho) {
    return this.ler(new File(caminho));
  }

  public Empresa ler(File caminho) {
    Empresa empresa = null;

    try {
      ObjectInputStream objIn = new ObjectInputStream(Files.newInputStream(caminho.toPath(), StandardOpenOption.READ));
      empresa = (Empresa) objIn.readObject();

    } catch (IOException | ClassNotFoundException ex) {
      Leitura.LOGGER.log(Level.SEVERE, null, ex);
    }

    return empresa;
  }
}
