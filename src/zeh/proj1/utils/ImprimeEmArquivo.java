package zeh.proj1.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import zeh.proj1.folhagenerica.Empresa;

/**
 *
 * @author jose-almeida
 */
public class ImprimeEmArquivo {

  private static final Logger LOGGER = Logger.getLogger(Leitura.class.getName());

  public void gerarFolhaPagamentoEmArquivo(String caminho, Empresa empresa) {
    this.gerarFolhaPagamentoEmArquivo(new File(caminho), empresa);
  }

  public void gerarFolhaPagamentoEmArquivo(File caminho, Empresa empresa) {
    try {
      caminho.createNewFile();
      FileWriter writer;

      writer = new FileWriter(caminho, true);
      writer.write(empresa.gerarFolha());
      writer.flush();
      writer.close();

    } catch (IOException ex) {
      ImprimeEmArquivo.LOGGER.log(Level.SEVERE, null, ex);
    }
  }
}
