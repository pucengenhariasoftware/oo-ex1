package zeh.proj1.folhainformatica;

import zeh.proj1.folhagenerica.Funcionario;
import zeh.proj1.folhagenerica.Horista;

/**
 *
 * @author jose-almeida
 */
public final class Programador extends Horista {

  public Programador(double salario, String nome, int codigo, int valorHora, int numeroHoras) {
    super(nome, codigo, valorHora, numeroHoras);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }
    final Programador other = (Programador) obj;

    if (this.getValorHora() != other.getValorHora()) {
      return false;
    }

    return this.getNumeroHoras() == other.getNumeroHoras();
  }
}
