package zeh.proj1.folhainformatica;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import zeh.proj1.folhagenerica.Empresa;
import zeh.proj1.folhagenerica.Funcionario;

/**
 *
 * @author jose-almeida
 */
public final class EmpresaInformatica extends Empresa {

  private List<Programador> programadores;
  private List<Analista> analistas;
  private List<Diretor> diretores;
  private List<Gerente> gerentes;

  public EmpresaInformatica() {
    super();
    this.iniciaListas();
  }

  public EmpresaInformatica(String nome) {
    super(nome);
    this.iniciaListas();
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 53 * hash + Objects.hashCode(this.getNome());

    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final EmpresaInformatica other = (EmpresaInformatica) obj;

    return this.getNome().equals(other.getNome());
  }

  @Override
  public List<Funcionario> getFuncionarios() {
    List<Funcionario> funcionarios = new ArrayList<>(this.programadores.size()
            + this.analistas.size()
            + this.diretores.size()
            + this.gerentes.size());

    funcionarios.addAll(this.programadores);
    funcionarios.addAll(this.analistas);
    funcionarios.addAll(this.diretores);
    funcionarios.addAll(this.gerentes);

    return funcionarios;
  }

  @Override
  public boolean addFuncionario(Funcionario funcionario) {
    if (funcionario == null) {
      throw new NullPointerException("Funcionário não declarado");
    }

    String tipo = funcionario.getClass().getName();

    if (tipo.equals(Analista.class.getName())) {
      return this.addAnalista((Analista) funcionario);

    } else if (tipo.equals(Programador.class.getName())) {
      return this.addProgramador((Programador) funcionario);

    } else if (tipo.equals(Gerente.class.getName())) {
      return this.addGerente((Gerente) funcionario);

    } else if (tipo.equals(Diretor.class.getName())) {
      return this.addDiretor((Diretor) funcionario);

    } else {
      throw new IllegalArgumentException(String.format("Funcionário do tipo %s não reconhecido", tipo));
    }
  }

  private void iniciaListas() {
    this.programadores = new ArrayList<>();
    this.analistas = new ArrayList<>();
    this.diretores = new ArrayList<>();
    this.gerentes = new ArrayList<>();
  }

  public boolean addProgramador(Programador programador) {
    return !this.programadores.contains(programador) && this.programadores.add(programador);
  }

  public boolean addAnalista(Analista analista) {
    return !this.analistas.contains(analista) && this.analistas.add(analista);
  }

  public boolean addDiretor(Diretor diretor) {
    return !this.diretores.contains(diretor) && this.diretores.add(diretor);
  }

  public boolean addGerente(Gerente gerente) {
    return !this.gerentes.contains(gerente) && this.gerentes.add(gerente);
  }

  public List<Programador> getProgramadores() {
    return new ArrayList<>(this.programadores);
  }

  public List<Analista> getAnalistas() {
    return new ArrayList<>(this.analistas);
  }

  public List<Diretor> getDiretores() {
    return new ArrayList<>(this.diretores);
  }

  public List<Gerente> getGerentes() {
    return new ArrayList<>(this.gerentes);
  }

  public Programador getProgramador(int codigo) {
    return (Programador) this.getFuncionario(this.programadores, codigo);
  }

  public Analista getAnalista(int codigo) {
    return (Analista) this.getFuncionario(this.analistas, codigo);
  }

  public Diretor getDiretor(int codigo) {
    return (Diretor) this.getFuncionario(this.diretores, codigo);
  }

  public Gerente getGerente(int codigo) {
    return (Gerente) this.getFuncionario(this.gerentes, codigo);
  }
}
