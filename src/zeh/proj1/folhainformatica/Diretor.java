package zeh.proj1.folhainformatica;

import zeh.proj1.folhagenerica.Mensalista;

/**
 *
 * @author jose-almeida
 */
public final class Diretor extends Mensalista {

  private double comissao;

  public Diretor(String nome, int codigo, double salarioMensal) {
    super(nome, codigo, salarioMensal);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }
    final Gerente other = (Gerente) obj;

    if (this.getComissao() != other.getComissao()) {
      return false;
    }

    return this.getSalarioMensal() == other.getSalarioMensal();
  }

  @Override
  public double getComissao() {
    return this.comissao;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 29 * hash + (int) (Double.doubleToLongBits(this.getComissao()) ^ (Double.doubleToLongBits(this.getComissao()) >>> 32));
    hash = 29 * hash + (int) (Double.doubleToLongBits(this.getSalarioMensal()) ^ (Double.doubleToLongBits(this.getSalarioMensal()) >>> 32));
    return hash;
  }

  public void setComissao(double comissao) {
    this.comissao = comissao;
  }
}
