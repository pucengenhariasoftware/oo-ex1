package zeh.proj1.folhagenerica;

import java.io.Serializable;

/**
 *
 * @author jose-almeida
 */
public abstract class Funcionario implements Serializable, Comparable<Funcionario> {
  
  private String nome;
  private int codigo;
  private String cargo;
  
  private Empresa empresa;
  
  public Funcionario() {
  }
  
  public Funcionario(String nome, int codigo) {
    this.setNome(nome);
    this.setCodigo(codigo);
  }
  
  @Override
  public int compareTo(Funcionario outro) {
    return this.hashCode() - outro.hashCode();
  }
  
  public final String getNome() {
    return this.nome;
  }
  
  public final void setNome(String nome) {
    this.nome = nome;
  }
  
  public final int getCodigo() {
    return this.codigo;
  }
  
  public final void setCodigo(int codigo) {
    this.codigo = codigo;
  }
  
  public final Empresa getEmpresa() {
    return this.empresa;
  }
  
  public final void setEmpresa(Empresa empresa) {
    this.empresa = empresa;
  }
  
  public final String getCargo() {
    return this.cargo;
  }
  
  public final void setCargo(String cargo) {
    this.cargo = cargo;
  }
  
  public String gerarContraCheque() {
    return this.toString();
  }
  
  public abstract double getSalario();
  
  @Override
  public abstract int hashCode();
  
  @Override
  public abstract boolean equals(Object object);
}
