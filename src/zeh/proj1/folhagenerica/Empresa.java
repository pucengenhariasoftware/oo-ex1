package zeh.proj1.folhagenerica;

import java.io.Serializable;
import java.util.List;
import zeh.proj1.utils.Escrita;

/**
 *
 * @author jose-almeida
 */
public abstract class Empresa implements Serializable, Comparable<Empresa> {

  private String nome;

  public Empresa() {
  }

  public Empresa(String nome) {
    this.setNome(nome);
  }

  public final String getNome() {
    return this.nome;
  }

  public final void setNome(String nome) {
    this.nome = nome;
  }

  protected Funcionario getFuncionario(List<? extends Funcionario> funcionarios, int codigo) {
    return (Funcionario) funcionarios.stream().filter(funcionario -> {
      return funcionario.getCodigo() == codigo;
    });
  }

  public String gerarFolha() {
    List<Funcionario> funcionarios = this.getFuncionarios();
    StringBuilder builder = new StringBuilder(funcionarios.size() * 100);

    builder.append(this.toString());
    builder.append("\n");

    funcionarios.stream().forEach((Funcionario funcionario) -> {
      builder.append(funcionario.gerarContraCheque());
      builder.append("\n");
    });

    return builder.toString();
  }

  public abstract List<Funcionario> getFuncionarios();

  public abstract boolean addFuncionario(Funcionario funcionario);

  @Override
  public int compareTo(Empresa outro) {
    return this.hashCode() - outro.hashCode();
  }

  @Override
  public String toString() {
    return String.format("%s//%s", Empresa.class.getSimpleName(), this.getNome()).replaceAll("//", Escrita.SEPARADOR);
  }

  @Override
  public abstract int hashCode();

  @Override
  public abstract boolean equals(Object object);
}
