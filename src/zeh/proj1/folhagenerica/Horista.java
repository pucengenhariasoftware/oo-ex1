package zeh.proj1.folhagenerica;

import zeh.proj1.utils.Escrita;

/**
 *
 * @author jose-almeida
 */
public abstract class Horista extends Funcionario {

  private int valorHora;
  private int numeroHoras;

  public Horista(String nome, int codigo, int valorHora, int numeroHoras) {
    super(nome, codigo);
    this.setValorHora(valorHora);
    this.setNumeroHoras(numeroHoras);
  }

  public final int getValorHora() {
    return this.valorHora;
  }

  public final void setValorHora(int valorHora) {
    this.valorHora = valorHora;
  }

  public int getNumeroHoras() {
    return this.numeroHoras;
  }

  public final void setNumeroHoras(int numeroHoras) {
    this.numeroHoras = numeroHoras;
  }

  @Override
  public final double getSalario() {
    return this.getNumeroHoras() * this.getValorHora();
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 79 * hash + this.getValorHora();
    hash = 79 * hash + this.getNumeroHoras();
    return hash;
  }

  @Override
  public String toString() {
    return String.format("%s//%s//%s//%d//%d//%d",
            Funcionario.class.getSimpleName(),
            this.getClass().getSimpleName(),
            this.getNome(),
            this.getCodigo(),
            this.getValorHora(),
            this.getNumeroHoras()).replaceAll("//", Escrita.SEPARADOR);
  }
}
