package zeh.proj1.folhagenerica;

import zeh.proj1.utils.Escrita;

/**
 *
 * @author jose-almeida
 */
public abstract class Mensalista extends Funcionario {

  private double salarioMensal;

  public Mensalista(String nome, int codigo, double salarioMensal) {
    super(nome, codigo);
    this.setSalarioMensal(salarioMensal);
  }

  public double getSalarioMensal() {
    return this.salarioMensal;
  }

  public final void setSalarioMensal(double salarioMensal) {
    this.salarioMensal = salarioMensal;
  }

  @Override
  public double getSalario() {
    return this.getSalarioMensal() + this.getComissao();
  }

  @Override
  public String toString() {
    return String.format("%s//%s//%s//%d//%f",
            Funcionario.class.getSimpleName(),
            this.getClass().getSimpleName(),
            this.getNome(),
            this.getCodigo(),
            this.getSalario()).replaceAll("//", Escrita.SEPARADOR);
  }

  public abstract double getComissao();
}
